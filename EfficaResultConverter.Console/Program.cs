﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EfficaResultConverter.Business;
using EfficaResultConverter.DataAccess;

namespace EfficaResultConverter.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 1; // Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["Count"]);

            if (args.Length > 0)
            {
                //if (args[0].ToUpper() == "RESTART")
                //{
                //    Properties.Settings.Default.TimeOfLastVisitHandled = "1900-01-01 23:59:59.999";
                //    Properties.Settings.Default.Save();
                //}
                if (!String.IsNullOrWhiteSpace(args[0].ToString()))
                {
                    if (args[0].ToUpper() == "RESTART")
                    {
                        Properties.Settings.Default.TimeOfLastVisitHandled = "1900-01-01 23:59:59.999";
                        Properties.Settings.Default.Save();
                    }
                    else if (args[0].ToUpper() == "ALL")
                    {
                        count = 0;
                    }
                    else
                    {
                        if (!Int32.TryParse(args[0], out count))
                            count = 1;
                    }
                }

                if (args.Length > 1)
                {
                    if (!String.IsNullOrWhiteSpace(args[1]))
                    {
                        if (args[1].ToUpper() == "RESTART")
                        {
                            Properties.Settings.Default.TimeOfLastVisitHandled = "1900-01-01 23:59:59.999";
                            Properties.Settings.Default.Save();
                        }
                        else if (args[1].ToUpper() == "ALL")
                        {
                            count = 0;
                        }
                        else
                        {
                            if (!Int32.TryParse(args[1], out count))
                                count = 1;
                        }
                    }
                }
            }

            int i = 0;

            if (count == 0)
            {
                // Tallennettu string muodossa, koska millisekunnit on saatava mukaan
                DateTime maxResultTime = DateTime.MinValue;
                DateTime.TryParseExact(Properties.Settings.Default.TimeOfLastVisitHandled, "yyyy-MM-dd HH:mm:ss.fff", null, DateTimeStyles.None, out maxResultTime);
                string testi = Properties.Settings.Default.TimeOfLastVisitHandled_DateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                // Luodaan käyntitunnisteet
                List<EfficaVisitInfo> newVisits = ResultConverter.LoadNewLabVisitIds(maxResultTime);

                while (newVisits.Count > 0)
                {
                    maxResultTime = ResultConverter.Start(maxResultTime, count, ref i, newVisits);
                    string saveTime = maxResultTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Properties.Settings.Default.TimeOfLastVisitHandled = saveTime;
                    Properties.Settings.Default.Save();
                    i++;

                    newVisits = ResultConverter.LoadNewLabVisitIds(maxResultTime);
                }
            }
            else
            {
                while (i < count)
                {
                    // Tallennettu string muodossa, koska millisekunnit on saatava mukaan
                    DateTime maxResultTime = DateTime.MinValue;
                    DateTime.TryParseExact(Properties.Settings.Default.TimeOfLastVisitHandled, "yyyy-MM-dd HH:mm:ss.fff", null, DateTimeStyles.None, out maxResultTime);

                    maxResultTime = ResultConverter.Start(maxResultTime, count, ref i, null);
                    string saveTime = maxResultTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Properties.Settings.Default.TimeOfLastVisitHandled = saveTime;
                    Properties.Settings.Default.Save();
                    i++;
                }
            }
        }
    }
}
