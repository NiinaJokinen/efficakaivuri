﻿using EfficaResultConverter.DataAccess;
using EfficaResultConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.Business
{
    public class ConverterLogger
    {
        public static void CreateSpecimenLog(List<Tuple<string, string>> Specimens, DateTime lastVisitResultTime)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePath"];
            var filename_specimens = $"{path}SpecimenIdDataLog_{Specimens.Distinct().ToList().Count()}specimens_LastVisitResultTime_{lastVisitResultTime.ToString("yyMMddhhmmfff")}_Time_{DateTime.Now.ToString("yyMMddhhmmfff")}";  
            var filename_specimens_persons = $"{path}SpecimenIdDataLog_{Specimens.Count()}datarows_LastVisitResultTime_{lastVisitResultTime.ToString("yyMMddhhmmfff")}_Time_{DateTime.Now.ToString("yyMMddhhmmfff")}"; 

            System.IO.StreamWriter stream_specimens = File.AppendText(string.Format("{0}.log", filename_specimens));
            System.IO.StreamWriter stream_specimens_persons = File.AppendText(string.Format("{0}.log", filename_specimens_persons));

            foreach (var v in Specimens)
            {
                string specimenId = String.Format("'{0}',",  v.Item1.ToString());
                // To file
                stream_specimens.WriteLine(specimenId);

                string specimenId_personID = String.Format("{0} - {1}", v.Item1.ToString(), v.Item2.ToString());
                // To file
                stream_specimens_persons.WriteLine(specimenId_personID);
            }

            stream_specimens.Flush();
            stream_specimens.Close();

            stream_specimens_persons.Flush();
            stream_specimens_persons.Close();


            // Erilliset näytteet tarkastuksia varten
            var filename_distinct_specimens = $"{path}SpecimenIdDataLog_{Specimens.Distinct().ToList().Count()}distinct_specimens_Time_{DateTime.Now.ToString("yyMMddhhmmfff")}"; 

            System.IO.StreamWriter stream_distinct_specimens = File.AppendText(string.Format("{0}.log", filename_distinct_specimens));


            foreach (var v in Specimens.Distinct())
            {
                string specimenId = String.Format("{0}", v.Item1.ToString());
                // To file
                stream_distinct_specimens.WriteLine(specimenId);
            }

            stream_distinct_specimens.Flush();
            stream_distinct_specimens.Close();

        }

        public static void CreateVisitIdLog(List<int> Visits)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePath"];
            var filename = $"{path}VisitIdDataLog_{Visits.Count()}visits_Time_{DateTime.Now.ToString("yyMMddhhmmfff")}";   // { DateTime.Now.ToString("yyMMddhhmmfff")}";

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.log", filename));

            foreach (var v in Visits)
            {
                string visitId = String.Format("'{0}',", v.ToString());
                // To file
                stream.WriteLine(visitId);
            }

            stream.Flush();
            stream.Close();
        }

        public static void CreateOrphanRowsLog(List<EfficaData> orphans)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePathOrphans"];
            var filename = $"{path}OrphanDataLogTime_{DateTime.Now.ToString("yyMMdd")}"; 

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.log", filename));

            stream.WriteLine($"Orphans LabvisitId: {orphans.First().LabVisitId} ");

            foreach (var o in orphans)
            {
                string orphanId = String.Format("{0}", o.WorkId.ToString());
                // To file
                stream.WriteLine(orphanId);
            }

            stream.WriteLine();

            stream.Flush();
            stream.Close();
        }

        public static void CreateWorkIdLog(List<int> Works, string path)
        {
            var filename = $"{path}WorkIdDataLog_Time_{DateTime.Now.ToString("yyMMdd")}";

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.log", filename));

            string workIdCount = String.Format("{0} workIds", Works.Count().ToString());
            // To file
            stream.WriteLine(workIdCount);

            foreach (var v in Works)
                {
                    string workId = String.Format("{0}", v.ToString());
                    // To file
                    stream.WriteLine(workId);
                }

            stream.Flush();
            stream.Close();
        }

        public static void CreatePatologySpecimensLog(List<int> patologySpecimens)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePath"];
            var filename = $"{path}PatologySpecimensDataLog_Time_{DateTime.Now.ToString("yyMMdd")}";

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.log", filename));

            string workIdCount = String.Format("{0} kpl poistettuja patologian näytteitä", patologySpecimens.Count().ToString());
            // To file
            stream.WriteLine(workIdCount);

            foreach (var v in patologySpecimens)
            {
                string workId = String.Format("'{0}',", v.ToString());
                // To file
                stream.WriteLine(workId);
            }

            stream.Flush();
            stream.Close();
        }

        public static void CreateErrorLog(List<EfficaVisitInfo> visitInfos)
        {
            List<EfficaVisitInfo> errors = visitInfos.Where(t => t.Status == "Epaonnistunut").Select(i => new EfficaVisitInfo() { LabVisitId = i.LabVisitId, Status = i.Status, ResultTime = i.ResultTime, Error = i.Error }).ToList();
            string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePath"];
            var filename = $"{path}VisitDataErrorLog_{errors.Count()}visits_{DateTime.Now.ToString("yyMMdd")}";

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.log", filename));

            stream.WriteLine($"VisitDataErrorLog: {errors.Count()} visits / {DateTime.Now.ToString("yyMMdd")}");
            stream.WriteLine();

            foreach (var e in errors)
            {
                string resultTime = e.ResultTime != null ? e.ResultTime.ToString() : DateTime.MinValue.ToString("G");
                string error = e.Error != null ? e.Error.ToString() : String.Empty;
                string visitInfo = e.LabVisitId.ToString() + '/' + e.Status.ToString() + '/' + resultTime + '/' + error;
                // To file
                stream.WriteLine(visitInfo);
                stream.WriteLine();
            }

            Console.WriteLine($"Muodostetaan käsitellyistä käynneistä log-tiedosto {filename}.");

            stream.Flush();
            stream.Close();
        }
    }
}
