﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EfficaResultConverter.Business.Models;
using EfficaResultConverter.DataAccess.Models;
using System.Configuration;

namespace EfficaResultConverter.Business
{
    public class DataParser : IDataParser
    {
        public LabVisit Parse(List<EfficaData> data, ref List<int> removedQpati)
        {
            // 4.5.2020 Palautetaan vanha käsittely tstiä varten
            // Jätetään patologian näytteet käsittelystä pois
            string QPati = ConfigurationManager.AppSettings["QPati"] != null ? ConfigurationManager.AppSettings["QPati"].Trim() : String.Empty;
            if (!String.IsNullOrWhiteSpace(QPati))
                RemoveQPati(ref data, QPati, ref removedQpati);

            if (data.Count > 0)
            {
                //Console.WriteLine($"Visit {data.First().LabVisitId}.");
                //Console.WriteLine($"data: {data.Count}.");

                var labVisit = new LabVisit();
                labVisit.PersonId = data[0].PersonId;
                labVisit.FirstName = data[0].FirstName;
                labVisit.LastName = data[0].LastName;
                // labVisit.DateOfBirth = Enersoft.Utils.Henkilo.Henkilotunnus.HaeSyntymaaika(data[0].PersonId);
                // labVisit.Gender = Enersoft.Utils.Henkilo.Henkilotunnus.HaeSukupuoli(data[0].PersonId);

                labVisit.OrgId = data[0].OrgId;
                labVisit.OrgUnit = data[0].OrgUnit;

                labVisit.LabVisitId = data[0].LabVisitId;

                // Luodaan toiminto, joka tarkastaa, että käynnin kaikki rivit käsitellään. 
                // Jos käynnillä on rivejä, joiden päätutkimustieto puuttuu, nämä yksittäiset rivit käsitellään erikseen ja niistä luodaan yksittäisiä sanomia
                List<int> treatedWorkIds = new List<int>();
                labVisit.LabSpecimens = ParseSpecimens(data, ref treatedWorkIds);
                //Console.WriteLine($"data1: {data.Count} / treated: {treatedWorkIds.Count}.");
                List<EfficaData> OrphanRows = new List<EfficaData>();
                // Onko käynnin kaikki tietokantarivit käsitelty?
                while (data.Count > treatedWorkIds.Count)
                {
                    //Console.WriteLine($"data2: {data.Count} / treated: {treatedWorkIds.Count}.");
                    foreach (var d in data)
                    {
                        if (!treatedWorkIds.Contains(d.WorkId))
                        {
                            d.MainResearchId = String.Empty;
                            OrphanRows.Add(d);
                        }
                    }

                    List<LabSpecimen> OrphanSpecimens = ParseSpecimens(OrphanRows, ref treatedWorkIds);
                    labVisit.LabSpecimens.AddRange(OrphanSpecimens);
                }
                if (OrphanRows.Count() > 0)
                    ConverterLogger.CreateOrphanRowsLog(OrphanRows);

                return labVisit;
            }
            else
            {
                return null;
            }
        }

        private static void RemoveQPati(ref List<EfficaData> data, string QPati, ref List<int> removedQpati)
        {
            if (QPati != null)
            {
                List<string> patologySpecimens = new List<string>() { };
                List<EfficaData> removedSpecimens = new List<EfficaData>() { };
                patologySpecimens = QPati.Split(',').ToList<string>();

                foreach (var d in data)
                {
                    if (patologySpecimens.Contains(d.ResearchId) || (patologySpecimens.Contains(d.MainResearchId)))
                    {
                        removedSpecimens.Add(d);
                        removedQpati.Add(d.WorkId);
                    }

                }
                foreach (var r in removedSpecimens)
                {
                    data.Remove(r);
                }
            }
        }

        private List<LabSpecimen> ParseSpecimens(List<EfficaData> data, ref List<int> treatedWorkIds)
        {
            var distinctSpecimens = GetDistinctMainSpecimens(data);
            if (distinctSpecimens.Count > 0)
            {
                List<LabSpecimen> labSpecimens = new List<LabSpecimen>();
                foreach (var spe in distinctSpecimens)
                {
                    var labSpecimen = ParseSpecimen(data, spe, ref treatedWorkIds);
                    labSpecimens.Add(labSpecimen);
                }
                return labSpecimens;
            }
            else
            {
                if (data.Count > 0)
                {
                    //Tänne tullaan, kun käynti koostuu vain osatutkimuksista --> muutetaan ne yksittäistutkimuksiksi ja käsitellään sellaisina
                    foreach (var d in data)
                        d.MainResearchId = String.Empty;

                    ConverterLogger.CreateOrphanRowsLog(data);

                    distinctSpecimens = GetDistinctMainSpecimens(data);

                    if (distinctSpecimens.Count > 0)
                    {
                        List<LabSpecimen> labSpecimens = new List<LabSpecimen>();
                        foreach (var spe in distinctSpecimens)
                        {
                            var labSpecimen = ParseSpecimen(data, spe, ref treatedWorkIds);
                            labSpecimens.Add(labSpecimen);
                        }
                        return labSpecimens;
                    }
                    else
                        return null;
                }
                return null;
            }
            //return null;
        }

        private LabSpecimen ParseSpecimen(List<EfficaData> data, EfficaData distinctSpecimen, ref List<int> treatedWorkIds)
        {
            var labSpecimen = new LabSpecimen();
            labSpecimen.SpecimenNumber = distinctSpecimen.SpecimenNumber;
            labSpecimen.SpecimenTime = distinctSpecimen.SpecimenTime;

            labSpecimen.LabPackets = GetLabPackets(data, distinctSpecimen.SpecimenNumber, distinctSpecimen.ResearchId, ref treatedWorkIds);

            return labSpecimen;
        }

        private List<LabResult> ParseLabResults(List<EfficaData> data, LabPacket labPacket, ref List<int> treatedWorkIds)
        {
            List<LabResult> labResults = new List<LabResult>();

            List<EfficaData> subSpecimens = GetSubSpecimens(data, labPacket);
            if (subSpecimens.Count > 0)
            {
                foreach (var subSpe in subSpecimens)
                {
                    var labResultSub = NewLabResult(subSpe);
                    labResults.Add(labResultSub);
                    if (!treatedWorkIds.Contains(subSpe.WorkId))
                        treatedWorkIds.Add(subSpe.WorkId);
                }
            }

            return labResults;
        }

        private LabResult NewLabResult(EfficaData specimen)
        {
            var labResult = new LabResult();
            labResult.ResearchId = specimen.ResearchId;
            labResult.ResearchDef = specimen.ResearchDef;
            //labResult.ResearchCode = ConfigurationManager.AppSettings["Tutkimuskoodisto"];
            labResult.ResearchText = specimen.ResearchText;
            labResult.Result = specimen.Result;
            labResult.Unit = specimen.Unit;
            labResult.RefValue = String.Empty;
            if (specimen.ResearchField != "P")
                labResult.RefValue = specimen.RefValue;
            labResult.AbnormalFlag = specimen.AbnormalFlag;
            labResult.ResultTime = specimen.ResultTime;
            labResult.SpecimenTime = specimen.SpecimenTime;
            labResult.ExceptionMsg = specimen.Notification;
            labResult.ResultStatus = MessageLibrary.Maaritykset.HL7.TutkimusvastauksenTulkintakoodi.F; // TODO : VARMISTA TULOKSEN TULKINTAKOODI !! MITEN TIEDETÄÄN ONKO MUUTTUNUT ??
                                                                                                       // LAUSUNTO OBXS ??
            if (!String.IsNullOrWhiteSpace(specimen.Statement))
                labResult.Statement = specimen.Statement;
            labResult.WorkId = specimen.WorkId;
            return labResult;
        }

        private List<EfficaData> GetSubSpecimens(List<EfficaData> data, LabPacket packet)
        {
            return data.Where(sub => sub.MainResearchId == packet.ResearchId || (sub.ResearchId == packet.ResearchId && String.IsNullOrWhiteSpace(sub.MainResearchId))).ToList<EfficaData>();
        }

        private List<LabPacket> GetLabPackets(List<EfficaData> data, string specimenNo, string researchId, ref List<int> treatedWorkIds)
        {
            List<LabPacket> labPackets = new List<LabPacket>();

            foreach (var dataRow in data)
            {
                if (dataRow.SpecimenNumber == specimenNo)
                    if (String.IsNullOrWhiteSpace(dataRow.MainResearchId) || dataRow.MainResearchId == dataRow.ResearchId)
                    {
                        labPackets.Add(CreateLabPacket(data, dataRow, false, ref treatedWorkIds));
                    }
            }

            // Orvot
            foreach (var dataRow in data)
            {
                if (dataRow.SpecimenNumber == specimenNo &&
                    !String.IsNullOrWhiteSpace(dataRow.MainResearchId) && // osatutkimukset
                    !data.Exists(t => t.ResearchId == dataRow.MainResearchId) && // joilta ei löydy päätutkimusta
                    !labPackets.Exists(p => p.ResearchId == dataRow.MainResearchId)) // joita ei ole vielä huomioitu
                {
                    labPackets.Add(CreateLabPacket(data, dataRow, true, ref treatedWorkIds));
                    //Console.WriteLine($"data: {dataRow.WorkId}.");
                }
            }

            return labPackets;
        }

        private LabPacket CreateLabPacket(List<EfficaData> data, EfficaData dataRow, bool ghost, ref List<int> treatedWorkIds)
        {
            var labPacket = new LabPacket
            {
                ResearchId = ghost ? dataRow.MainResearchId : dataRow.ResearchId,
                ResearchDef = ghost ? dataRow.MainResearchDef : dataRow.ResearchDef,
                //labPacket.ResearchCode = ConfigurationManager.AppSettings["Tutkimuskoodisto"];
                SpecimenTime = dataRow.SpecimenTime,
                ResultTime = dataRow.ResultTime
            };
            labPacket.LabResults = ParseLabResults(data, labPacket, ref treatedWorkIds);
            return labPacket;
        }

        private List<EfficaData> GetDistinctMainSpecimens(List<EfficaData> data)
        {
            List<EfficaData> mainSpecimens = new List<EfficaData>();
            foreach (var i in data)
            {
                if (String.IsNullOrWhiteSpace(i.MainResearchId) || i.MainResearchId == i.ResearchId) // || !data.Exists(t => t.ResearchId == i.MainResearchId))
                {
                    mainSpecimens.Add(i);
                    //Console.WriteLine($"Main specimen workid {i.WorkId}.");
                }
            }

            List<EfficaData> distinctMainSpecimens = mainSpecimens
                .GroupBy(specimen => specimen.SpecimenNumber)
                .Select(mainSpecimen => mainSpecimen.First())
                .ToList();

            return distinctMainSpecimens;
        }

    }
}
