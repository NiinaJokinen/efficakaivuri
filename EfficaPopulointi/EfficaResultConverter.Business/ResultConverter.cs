﻿using EfficaResultConverter.Business.Models;
using EfficaResultConverter.DataAccess;
using EfficaResultConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Globalization;

namespace EfficaResultConverter.Business
{
    public class ResultConverter
    {
        public object ConfigurationManager { get; private set; }
        //const string strConn = "SERVER=192.168.83.47;DATABASE=EfficaLabraTesti;UID=equ;PWD=siivikkala;";

        public enum visitStatus
        {
            Kasittelematta,
            Kasittelyssa,
            Kasitelty,
            Epaonnistunut
        }
        public static DateTime Start(DateTime maxResultTime, int count, ref int i, List<EfficaVisitInfo> newVisits)
        {
            // START
            Console.WriteLine($"Effica hl7 laturi käynnistyi. {DateTime.Now}");

            Console.WriteLine($"TimeOfLastVisitHandled: {maxResultTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}");

            if (newVisits == null)
            {
                // HAETAAN KÄYNNIT
                Stopwatch labVisitSW = new Stopwatch();
                labVisitSW.Start();

                // Luodaan käyntitunnisteet
                newVisits = LoadNewLabVisitIds(maxResultTime);
                Console.WriteLine($"Käsiteltäviä käyntejä: {newVisits.Count()} kpl");

                labVisitSW.Stop();
                Console.WriteLine($"LoadNewLabVisitIds kesti {labVisitSW.ElapsedMilliseconds / 1000}s");
            }

            int msgId_number = 1;

            if (newVisits.Count() > 0)
            {
                Stopwatch orusProcessedSW = new Stopwatch();
                orusProcessedSW.Start();

                // LUODAAN KÄYNTIEN HL7 SANOMAT

                // HL7s to list
                List<Tuple<string, string>> listSpecimens = new List<Tuple<string, string>>();
                List<int> visitIds = new List<int>();
                // Listataan testiä varten jokaisen rivin työtunnistenumero, jotta voidaan seurata tietokannasta käsiteltävien rivien lukumäärää
                List<int> listWorkIds = new List<int>();

                // Listataan testiä varten jokaisen käsittelemättä jääneen patologian tutkimuksen tutkimusnumero, jotta voidaan seurata tietokannasta poistettujen patologian tutkimusten määrää
                List<int> listPatologySpecimens = new List<int>();
                DateTime lastVisitResultTime = DateTime.MinValue;
                int countVisits = 0;
                List<EfficaOru> HL7s = CreateOrus(ref newVisits, ref listSpecimens, ref visitIds, ref lastVisitResultTime, ref listWorkIds, ref listPatologySpecimens, ref countVisits, ref msgId_number);
                Console.WriteLine($"HL7 -sanomia muodostettu: {HL7s.Count()} kpl");
                Console.WriteLine($"Näytteitä käsitelty: {listSpecimens.Distinct().ToList().Count()} kpl");


                // HL7:T TIETOSTOON
                OrusToFile(ref HL7s, listSpecimens, listWorkIds, countVisits);

                // TIETOKANTATALLENNUKSET

                //SaveLabVisitIds(newVisits);
                //SaveOrusToDatabase(HL7s);

                //DatabaseInserts.Stop();
                //Console.WriteLine($"DatabaseInserts kesti {DatabaseInserts.ElapsedMilliseconds / 1000}s");


                // LOG
                // Luodaan Log-tiedosto käsitellyistä käynneistä
                //ConverterLogger.CreateLog(newVisits);
                DateTime maxTime = GetMaxResultTime(newVisits, lastVisitResultTime);
                ConverterLogger.CreateErrorLog(newVisits);
                ConverterLogger.CreateSpecimenLog(listSpecimens, maxTime);
                ConverterLogger.CreateVisitIdLog(visitIds);
                string path = System.Configuration.ConfigurationManager.AppSettings["LogFilePathWorkId"];
                ConverterLogger.CreateWorkIdLog(listWorkIds, path);
                ConverterLogger.CreatePatologySpecimensLog(listPatologySpecimens);


                orusProcessedSW.Stop();
                Console.WriteLine($"HL7 -käsittely kesti {orusProcessedSW.ElapsedMilliseconds / 1000}s");

                // THE END
                Console.WriteLine($"Effica hl7 laturi loppui. {DateTime.Now}");

                Console.WriteLine($"Max. kuittausaika: {maxTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
                Console.WriteLine();

                return maxTime;
            }
            else
            {
                Console.WriteLine($"Ei uusia käyntejä. Käsitelty {i}/{count} kierrosta.");
                return maxResultTime;
            }
        }

        private static DateTime GetMaxResultTime(List<EfficaVisitInfo> newVisits, DateTime lastVisitResultTime)
        {
            DateTime maxTime = DateTime.MinValue;

            if (lastVisitResultTime != DateTime.MinValue)
                maxTime = DateTime.Parse(lastVisitResultTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            else
            {
                DateTime maxResultTime = newVisits.Max(t => t.ResultTime);
                if (maxResultTime != null)
                    maxTime = (DateTime)maxResultTime;
            }

            return maxTime;
        }

        public static List<EfficaVisitInfo> LoadNewLabVisitIds(DateTime maxResultTime)
        {
            // Max kuittausaika
            IEfficaVisitRepo visitR = new EfficaVisitRepo();

            // Haetaan kaikki kuittausajan jälkeen tulleet uudet käyntitunnukset
            IEfficaDataRepo dataR = new EfficaDataRepo();
            List<EfficaData> newEfficaDatas = dataR.GetNewLabVisitIds(maxResultTime);

            List<EfficaVisitInfo> newVisits = new List<EfficaVisitInfo>();

            foreach (var n in newEfficaDatas)
            {
                EfficaVisitInfo newVisit = new EfficaVisitInfo();
                newVisit.LabVisitId = n.LabVisitId;
                newVisit.ResultTime = n.ResultTime;
                newVisit.Status = visitStatus.Kasittelematta.ToString();
                newVisit.StartTime = null;
                newVisit.EndTime = null;
                // Lisätään uudet käyntitunnukset yksi kerrallaan
                newVisits.Add(newVisit);
            }



            return newVisits;

            // System.Configuration.ConfigurationManager.AppSettings["EfficaDbConnectionString"].ToString();
            // Lisätään lista massana tietokantaan
            // if (newVisits.Count > 0)
            //  visitR.BulkInsert(newVisits);

        }

        private static void SaveLabVisitIds(List<EfficaVisitInfo> newVisits)
        {
            IEfficaVisitRepo visitR = new EfficaVisitRepo();

            if (newVisits.Count > 0)
                visitR.BulkInsert(newVisits);
        }
        private static void SaveOrusToDatabase(List<EfficaOru> HL7s)
        {
            IEfficaOruRepo oruR = new EfficaOruRepo();
            if (HL7s.Count > 0)
                oruR.BulkInsert(HL7s);
        }

        //private static void TestOruToDatabase(int LabVisitId)
        //{
        //    IEfficaDataRepo visitDatas = new EfficaDataRepo();
        //    List<EfficaData> efficaVisitDatas = visitDatas.GetResultData(LabVisitId);
        //    if (efficaVisitDatas != null)
        //    {
        //        // LabVisit
        //        LabVisit labVisit = GetLabVisit(efficaVisitDatas);
        //        // HL7 to Database
        //        BuildOrus(labVisit);
        //        // Update VisitInfo
        //        UpdateVisitStatus(LabVisitId, visitStatus.Kasitelty.ToString(), DateTime.Now, DateTime.Now);

        //    }
        //}

        private static List<EfficaOru> CreateOrus(ref List<EfficaVisitInfo> visits, ref List<Tuple<string, string>> listSpecimens, ref List<int> visitIds, ref DateTime lastVisitResultTime, ref List<int> listWorkIds, ref List<int> removedQpati, ref int countVisits, ref int msgId_number)
        {
            // GET VISITS
            //IEfficaVisitRepo visits = new EfficaVisitRepo();
            //List<EfficaVisitInfo> efficaVisits = visits.GetEfficaVisits(visitStatus.Kasittelematta.ToString());

            List<EfficaOru> HL7s = new List<EfficaOru>();

            if (visits.Count() != 0)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                // Haetaan käyntien tiedot
                IEfficaDataRepo visitData = new EfficaDataRepo();
                List<EfficaData> EfficaVisitDatas = new List<EfficaData>();

                int howManyPerCycle = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["sqlVisitsPerCycle"]);
                List<EfficaVisitInfo> copyVisits = visits.ToList();
                int i = 0;

                while (copyVisits.Count > 0)
                {
                    int howManyToProcess = howManyPerCycle;
                    if (copyVisits.Count < howManyPerCycle)
                        howManyToProcess = copyVisits.Count;

                    var x = visits.GetRange(i * howManyPerCycle, howManyToProcess);
                    List<EfficaData> efficaVisitData = visitData.GetResultData(x.Select(v => v.LabVisitId).ToList());
                    EfficaVisitDatas.AddRange(efficaVisitData);

                    copyVisits.RemoveRange(0, howManyToProcess);
                    i++;
                }

                List<string> msgId_memory = new List<string>();
                int int_memory = 1;

                // Ei käsitellä koko käyntijoukkoa, vaan jätetään lopusta muutama (RemovedVisitCount) käynti käsittelemättä
                // Tämä siksi, että voidaan käsitellä kuittausajat kerralla loppuun
                int removedVisits = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxVisitCount"]) - Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["RemovedVisitCount"]);
                foreach (var efficaVisit in visits)
                {
                    var visitDatat = EfficaVisitDatas.Where(e => e.LabVisitId == efficaVisit.LabVisitId);

                    if (visitDatat != null && visitDatat.Count() > 0)
                    {
                        if (countVisits < removedVisits)
                        {
                            try
                            {
                                var visitHL7s = CreateVisitOrus(efficaVisit, visitDatat.ToList(), ref listWorkIds, ref msgId_memory, ref int_memory, ref listSpecimens, ref removedQpati, ref msgId_number);
                                HL7s.AddRange(visitHL7s);
                                efficaVisit.Status = visitStatus.Kasitelty.ToString();
                                lastVisitResultTime = efficaVisit.ResultTime;
                                visitIds.Add(efficaVisit.LabVisitId);
                            }
                            catch (Exception e)
                            {
                                // Error
                                efficaVisit.Status = visitStatus.Epaonnistunut.ToString();
                                efficaVisit.Error = e.ToString();
                                lastVisitResultTime = efficaVisit.ResultTime;
                                Console.WriteLine($"error {e.Message.ToString()}.");
                                continue;
                            }
                            // Nyt tänne lisätään myös siinä tapauksessa, jos käynti sisältää pelkkiä QPati -tutkimuksia, eikä sitä käsitellä
                            countVisits++;
                            //visitIds.Add(efficaVisit.LabVisitId);
                        }
                        else
                        {
                            if (efficaVisit.ResultTime == lastVisitResultTime)
                            {
                                try
                                {
                                    var visitHL7s = CreateVisitOrus(efficaVisit, visitDatat.ToList(), ref listWorkIds, ref msgId_memory, ref int_memory, ref listSpecimens, ref removedQpati, ref msgId_number);
                                    HL7s.AddRange(visitHL7s);
                                    efficaVisit.Status = visitStatus.Kasitelty.ToString();
                                    lastVisitResultTime = efficaVisit.ResultTime;
                                    visitIds.Add(efficaVisit.LabVisitId);
                                }
                                catch (Exception e)
                                {
                                    // Error
                                    efficaVisit.Status = visitStatus.Epaonnistunut.ToString();
                                    efficaVisit.Error = e.ToString();
                                    lastVisitResultTime = efficaVisit.ResultTime;
                                    Console.WriteLine($"error {e.Message.ToString()}.");
                                    continue;
                                }
                                // Nyt tänne lisätään myös siinä tapauksessa, jos käynti sisältää pelkkiä QPati -tutkimuksia, eikä sitä käsitellä
                                countVisits++;
                                //visitIds.Add(efficaVisit.LabVisitId);
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                }


                var ascendingOrder = listWorkIds.OrderBy(h => h);

                sw.Stop();
                Console.WriteLine($"Orujen luonti kesti {sw.ElapsedMilliseconds / 1000}s.");
                Console.WriteLine($"Käsiteltyjä käyntejä {countVisits}.");
                Console.WriteLine($"Läpikäytyjä työtunnisteita {listWorkIds.Count()}kpl.");
                if (countVisits > removedVisits)
                    Console.WriteLine($"Viimeisellä kuittausajalla, {lastVisitResultTime}, useampi käynti : {countVisits} / {removedVisits}.");
                //SaveOrusToDatabase(HL7s);
                return HL7s;
            }
            else
            {
                Console.WriteLine("Käyntejä ei löytynyt.");
                return new List<EfficaOru>();
            }
        }

        private static List<EfficaOru> CreateVisitOrus(EfficaVisitInfo efficaVisit, List<EfficaData> efficaVisitData, ref List<int> listWorkIds, ref List<string> msgId_memory, ref int int_memory, ref List<Tuple<string, string>> listSpecimens, ref List<int> removedQpati, ref int msgId_number)
        {
            // LabVisit
            LabVisit labVisit = GetLabVisit(efficaVisitData, ref removedQpati);

            if (labVisit != null)
            {
                // HL7 to Database
                var hl7s = BuildOrus(labVisit, ref listWorkIds, ref msgId_memory, ref int_memory, ref listSpecimens, ref msgId_number);
                return hl7s;
            }
            else
            {
                return new List<EfficaOru>();
            }
        }

        // Luodaan HL7 filet
        private static List<EfficaOru> BuildOrus(LabVisit labVisit, ref List<int> listWorkIds, ref List<string> msgId_memory, ref int int_memory, ref List<Tuple<string, string>> listSpecimens, ref int msgId_number)
        {
            
            if (labVisit.LabSpecimens != null)
            {
                return MessageFactory.CreateMessage(labVisit, ref listWorkIds, ref msgId_memory, ref int_memory, ref listSpecimens, ref msgId_number);

                // HL7 to Database
                //IEfficaOruRepo oru = new EfficaOruRepo();
                //oru.BulkInsert(hl7datas);
            }
            else
                return new List<EfficaOru>();
        }

        private static void OrusToFile(ref List<EfficaOru> hl7s, List<Tuple<string, string>> listSpecimens, List<int> listWorkIds, int countVisits)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["hl7FilePath"];
            int perFile = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxOruCount"]);
            int count = 0;
            int iteration = 0;

            // To File
            var filename = $"{path}hl7_{DateTime.Now.ToString("yyMMddhhmmssfff")}_hl7_{hl7s.Count()}_specimens_{listSpecimens.Distinct().ToList().Count()}";
            //var filename = $"{path}hl7_{DateTime.Now.ToString("yyMMddhhmmfff")}";
            Console.WriteLine($"Luettu {hl7s.Count()} sanomaa. Muodostetaan tiedosto {filename}.");

            System.IO.StreamWriter stream = File.AppendText(string.Format("{0}.txt", filename));
            System.IO.StreamWriter streamData = File.AppendText($"{path}hl7_{DateTime.Now.ToString("yyMMdd")}_data.txt");

            string Count = String.Format("{0} hl7s - {1} specimens - {2} visits - {3} workids / First Specimen - LabVisitId: {4} - {5}", hl7s.Count().ToString(), listSpecimens.Distinct().ToList().Count().ToString(), countVisits, listWorkIds.Count(), hl7s.FirstOrDefault().SpecimenNumber.ToString(), hl7s.FirstOrDefault().LabVisitId.ToString());
            // To file
            streamData.WriteLine(Count);

            foreach (var o in hl7s)
            {
                count++;

                // To file
                stream.Write(o.HL7);
                //stream.WriteLine(); // TODO : Varmista NewLine tarve ?

                // Update Status
                o.Processed = true;
                o.ProcessedTime = DateTime.Now;
                //oru.UpdateEfficaOruProcessed(o.EfficaOruKey);

                if (count == perFile)
                {
                    count = 0;
                    stream.Flush();
                    stream.Close();
                    iteration++;
                    stream = File.AppendText(string.Format("{0}_{1}.hl7", filename, iteration));

                    Console.WriteLine($"Muodostetaan tiedosto {filename}.");
                }

                //hl7s = oru.GetUnProcessedOrus(hl7s.Max(o => o.EfficaOruKey));
            }

            stream.Flush();
            stream.Close();

            streamData.Flush();
            streamData.Close();
        }

        private static void OrusToFile()
        {
            IEfficaOruRepo oru = new EfficaOruRepo();
            string path = System.Configuration.ConfigurationManager.AppSettings["hl7FilePath"];
            var orus = oru.GetUnProcessedOrus(30000);

            while (orus.Count() != 0)
            {
                // To File
                var filename = $"{path}hl7_{DateTime.Now.ToString("yyMMddhhmmfff")}";
                Console.WriteLine($"Luettu {orus.Count()} sanomaa. Muodostetaan tiedosto {filename}.");

                using (System.IO.StreamWriter stream = File.AppendText(filename))
                {
                    foreach (var o in orus)
                    {
                        // To file
                        stream.WriteLine(o.HL7);
                        //stream.WriteLine(); // TODO : Varmista NewLine tarve ?

                        // Update Status
                        //oru.UpdateEfficaOruProcessed(o.EfficaOruKey);
                    }
                }

                orus = oru.GetUnProcessedOrus(orus.Max(o => o.EfficaOruKey));
            }
        }

        private static EfficaVisitInfo UpdateVisitStatus(int visitId, string status, DateTime start_time, DateTime end_time)
        {
            IEfficaVisitRepo visit = new EfficaVisitRepo();
            EfficaVisitInfo efficaVisit = visit.GetEfficaVisit(visitId);

            efficaVisit.Status = status;
            if (start_time != DateTime.MinValue)
                efficaVisit.StartTime = start_time;
            if (end_time != DateTime.MinValue)
                efficaVisit.EndTime = end_time;
            visit.UpdateEfficaVisit(efficaVisit);

            return efficaVisit;
        }

        //private static List<EfficaData> GetVisitDatas(int visitId)
        //{
        //    IEfficaDataRepo data = new EfficaDataRepo();
        //    List<EfficaData> efficaVisitDatas = new List<EfficaData>();

        //    efficaVisitDatas = data.GetResultData(visitId);
        //    return efficaVisitDatas;
        //}

        private static LabVisit GetLabVisit(List<EfficaData> efficaVisitDatas, ref List<int> removedQpati)
        {
            IDataParser parser = new DataParser();
            LabVisit labVisit = parser.Parse(efficaVisitDatas, ref removedQpati);
            return labVisit;
        }

        public static void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
