﻿

using System.Collections.Generic;

namespace EfficaResultConverter.Business.Models
{
    public class LabVisit
    {
        public int LabVisitId { get; set; } // ??
        // PID data
        public string PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender  { get; set; }
        // PV1 data
        public string OrgId { get; set; }
        public string OrgUnit { get; set; }
        // Specimens
        public List<LabSpecimen> LabSpecimens { get; set; }

    }
}
