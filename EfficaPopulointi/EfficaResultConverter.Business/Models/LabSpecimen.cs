﻿using System;
using System.Collections.Generic;


namespace EfficaResultConverter.Business.Models
{
    public class LabSpecimen
    {
        // ORC data
        public string SpecimenNumber { get; set; }
        public DateTime SpecimenTime { get; set; }

        public List<LabPacket> LabPackets { get; set; }

    }
}
