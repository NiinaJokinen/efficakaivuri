﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MessageLibrary.Maaritykset;

namespace EfficaResultConverter.Business.Models
{
    public class LabResult
    {
        // OBX
        public string ResearchId { get; set; }
        public string ResearchDef { get; set; }
        public string ResearchText { get; set; }
        public string Result { get; set; }
        public string Unit { get; set; }
        public string RefValue { get; set; }
        public string AbnormalFlag { get; set; }   // Ylitys/poikkeama
        public DateTime SpecimenTime { get; set; }
        public DateTime ResultTime { get; set; }
        public string ExceptionMsg { get; set; }
        public string Statement { get; set; }
        public HL7.TutkimusvastauksenTulkintakoodi ResultStatus { get; internal set; }

        // Testiä varten
        public int WorkId { get; set; }
    }
}
