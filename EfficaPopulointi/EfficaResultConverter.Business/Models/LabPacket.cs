﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.Business.Models
{
    public class LabPacket
    {
        // OBR
        public string ResearchId { get; set; }
        public string ResearchDef { get; set; }
        public DateTime SpecimenTime { get; set; }
        public DateTime ResultTime { get; set; }
        public List<LabResult> LabResults { get; set; }

    }
}
