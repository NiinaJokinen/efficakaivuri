﻿using System;
using System.Collections.Generic;
using EfficaResultConverter.Business.Models;
using EfficaResultConverter.DataAccess.Models;
using MessageLibrary.HL7;
using MessageLibrary.HL7.Model;
using MessageLibrary.Vakiot.HL7;
using System.Configuration;
using System.Linq;


namespace EfficaResultConverter.Business
{
    internal class MessageFactory
    {
        internal static List<EfficaOru> CreateMessage(LabVisit labVisit, ref List<int> listWorkIds, ref List<string> msgId_memory, ref int int_memory, ref List<Tuple<string, string>> listSpecimens, ref int msgId_number)
        {
            var messagedatas = new List<EfficaOru>();
            foreach (var specimen in labVisit.LabSpecimens)
            {
                EfficaOru msgData = new EfficaOru();
                msgData.LabVisitId = labVisit.LabVisitId;
                msgData.SpecimenNumber = specimen.SpecimenNumber;

                var hl7 = CreateMessage(ref msgId_number, labVisit, specimen, ref listWorkIds, ref msgId_memory, ref int_memory);

                if (hl7 != null)
                {
                    // HL7XML
                    // msgData.HL7 = Enersoft.Utils.Xml.SerializationUtil.ToXml<HL7LABCONTENT>(hl7);
                    // HL7
                    var converter = new MessageLibrary.HL7.Convert.FromHL7XML();
                    msgData.HL7 = converter.ConvertMessage(hl7);

                    msgData.Processed = false;
                    msgData.ProcessedTime = DateTime.MinValue;

                    messagedatas.Add(msgData);

                    // specimenid - personid
                    Tuple<string, string> specimen_person = new Tuple<string, string>(specimen.SpecimenNumber, labVisit.PersonId);
                    listSpecimens.Add(specimen_person);
                }
            }

            return messagedatas;
        }

        private static HL7LABCONTENT CreateMessage(ref int msgId_number, LabVisit labVisit, LabSpecimen specimen, ref List<int> listWorkIds, ref List<string> msgId_memory, ref int int_memory)
        {
            var hl7 = CreateHl7LabContent(ref msgId_number, ref msgId_memory, ref int_memory);
            CreatePid(hl7, labVisit);
            CreatePv1(hl7, labVisit);
            CreateOrc(hl7, specimen);
            CreateLabRequests(hl7, specimen, ref listWorkIds); //OBRs
            return hl7;

        }

        private static bool CreateLabRequests(HL7LABCONTENT hl7, LabSpecimen specimen, ref List<int> listWorkIds)
        {
            var OBRIndeksi = -1;
            var OBXIndeksi = -1;

            bool processed = false;

            foreach (var packet in specimen.LabPackets)
            {
                string tutkimuskoodisto = "LAB-KL-98";

                // OBR
                var OBR =
                        HL7Muodostus.MuodostaOBR(OBRIndeksi,
                        specimen.SpecimenNumber,
                        null,
                        null,
                        null,
                        packet.ResearchId,
                        packet.ResearchDef,
                        tutkimuskoodisto,
                        specimen.SpecimenTime,
                        null,
                        MessageLibrary.Maaritykset.HL7.Naytteenottotoiminto.L.ToString(),
                        packet.ResultTime,  // TODO: kuittausaika eli ResultTime - ei packet.SpecimenTime
                        MessageLibrary.Maaritykset.HL7.TuottajanTyyppi.LAB,
                        (MessageLibrary.Maaritykset.HL7.TuloksenTila)
                        Enum.Parse(typeof(MessageLibrary.Maaritykset.HL7.TuloksenTila),
                        MessageLibrary.Maaritykset.HL7.TuloksenTila.F.ToString()));

                HL7Utils.LisaaOBR(OBR, ref OBRIndeksi, ref OBXIndeksi, ref hl7);

                CreateLabObservations(ref hl7, OBRIndeksi, ref OBXIndeksi, packet, tutkimuskoodisto, ref listWorkIds);

                processed = true;

            }

            return processed;
        }

        private static void CreateLabObservations(ref HL7LABCONTENT hl7, int OBRIndeksi, ref int OBXIndeksi, LabPacket packet, string tutkimuskoodisto, ref List<int> listWorkIds)
        {
            foreach (var result in packet.LabResults)
            {
                // STATEMENT
                bool statement = false;
                if (String.IsNullOrWhiteSpace(result.Statement))
                    statement = true;

                // OBX
                var tietotyyppi = HL7Utils.PaatteleHL7Tietotyyppi(result.Result, "."); // Desimaalierotin valittuna nyt .
                var OBX = HL7Muodostus.MuodostaOBX(OBXIndeksi, tietotyyppi,
                    result.ResearchId,
                    result.ResearchDef,
                    tutkimuskoodisto,
                    result.Result,
                    result.Unit,
                    result.ResultStatus,
                    result.SpecimenTime, // TODO : Tähän pitää saada kuittausaika eli SpecimenTime, ei ResultTime,
                    null,
                    "HL7", // TODO : ONKO OK, ETTÄ MUUTTAJAKSI MENEE HL7 ??
                    null);

                OBX.OBX3.CE5 = result.ResearchText;
                OBX.OBX7 = result.RefValue;
                OBX.OBX8 = result.AbnormalFlag == "*" ? Poikkeustilanneviesti.PoikkeavaArvo.ToString() : String.Empty;

                HL7Utils.LisaaOBX(OBX, OBRIndeksi, ref OBXIndeksi, ref hl7);

                CreateStatement(OBRIndeksi, ref OBXIndeksi, ref hl7, result);

                if (!listWorkIds.Contains(result.WorkId))
                    // Työtunniste testiä varten
                    listWorkIds.Add(result.WorkId);
            }
        }

        private static void CreateStatement(int OBRIndeksi, ref int OBXIndeksi, ref HL7LABCONTENT hl7, LabResult result)
        {

            if (String.IsNullOrWhiteSpace(result.Statement))
                return;

            var statements = result.Statement.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            foreach (var statement in statements)
            {
                HL7Utils.LisaaOBX(Koodisto.Lausunto, Lyhenne.Lausunto, Tunniste.Lausunto,
                    statement, MessageLibrary.Maaritykset.HL7.Tietotyyppi.TX,
                    OBRIndeksi, ref OBXIndeksi, ref hl7);
            }
        }

        private static void CreateOrc(HL7LABCONTENT hl7, LabSpecimen specimen)
        {
            hl7.HL7LABBODY.HL7LABORDER.ORC = HL7Muodostus.MuodostaORC(MessageLibrary.Maaritykset.HL7.TilauksenToimintokoodi.RE,
                specimen.SpecimenNumber,
                null,
                null,
                null,
                MessageLibrary.Maaritykset.HL7.TilauksenTila.CM,
                specimen.SpecimenTime,
                null,
                null,
                null);
        }

        private static void CreatePv1(HL7LABCONTENT hl7, LabVisit labVisit)
        {
            hl7.HL7LABBODY.PV1 = HL7Muodostus.MuodostaPV1(labVisit.OrgUnit,
                null,
                null,
                null, //labVisit.OrgId, korjattu JS ohjeiden mukaisesti 16.4.2019
                null);
        }

        private static void CreatePid(HL7LABCONTENT hl7, LabVisit labVisit)
        {
            hl7.HL7LABBODY.PID = HL7Muodostus.MuodostaPID(labVisit.PersonId,
                MessageLibrary.Vakiot.HL7.Tunniste.Henkilotunnus,
                null,
                labVisit.LastName,
                labVisit.FirstName,
                null,
                labVisit.DateOfBirth,
                labVisit.Gender);
        }

        private static HL7LABCONTENT CreateHl7LabContent(ref int msgId_number, ref List<string> msgId_memory, ref int int_memory)
        {
            // Lisätään tähän juokseva nro, jota kasvatetaan aina 
            var msgId = $"{DateTime.Now.ToString("yyMMddHHmmssfff")}_{msgId_number}";
            msgId_number++;

            if (msgId_memory.Contains(msgId))
            {
                msgId = $"{msgId}_{int_memory}";
                int_memory++;
                msgId_memory.Add(msgId);
            }
            else if (!msgId_memory.Contains(msgId))
            {
                int_memory = 1;
                msgId_memory.Add(msgId);
            }
            else
                msgId_memory.Add(msgId);

            // lahettava
            HD sendingApp = GetMshAddress("SendingSystem", "SendingEnvironment");
            // lahettavaTarkenne
            HD sendingFacility = GetMshAddress("SendingSystem", "SendingOrganization");
            // vastaanottava
            HD recevingApp = GetMshAddress("ReceivingSystem", "ReceivingEnvironment");
            // vastaanottavaTarkenne
            HD recevingFacility = GetMshAddress("ReceivingSystem", "ReceivingOrganization");

            var hl7 = new HL7LABCONTENT();
            hl7.MSH = HL7Muodostus.MuodostaMSH(sendingApp, sendingFacility, recevingApp, recevingFacility, MessageLibrary.Maaritykset.HL7.Sanomatyyppi.ORU,
                MessageLibrary.Maaritykset.HL7.SanomatyyppiLiipasin.R01,
                msgId, MessageLibrary.Maaritykset.HL7.Tuotantotyyppi.P,
                MessageLibrary.Maaritykset.HL7.KuittausTarve.AL);

            hl7.HL7LABBODY = new HL7LABBODYCONTENT
            {
                HL7LABORDER = new HL7LABORDERCONTENT()
            };

            return hl7;
        }

        private static HD GetMshAddress(string systemNameKey, string systemIdKey)
        {
            return new HD
            {
                HD1 = System.Configuration.ConfigurationManager.AppSettings[systemNameKey],
                HD2 = System.Configuration.ConfigurationManager.AppSettings[systemIdKey]
            };
        }
    }
}