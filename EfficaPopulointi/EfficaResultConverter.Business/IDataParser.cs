﻿using System.Collections.Generic;
using EfficaResultConverter.Business.Models;
using EfficaResultConverter.DataAccess.Models;

namespace EfficaResultConverter.Business
{
    public interface IDataParser
    {
        LabVisit Parse(List<EfficaData> data, ref List<int> removedQpati);
    }
}