﻿using EfficaResultConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace EfficaResultConverter.DataAccess
{
    public interface IEfficaVisitRepo
    {
        // EfficaVisitInfo
        List<EfficaVisitInfo> GetEfficaVisits(string status);
        EfficaVisitInfo GetEfficaVisit(int visitId);
        void UpdateEfficaVisit(EfficaVisitInfo visit);
        DateTime? GetMaxResultTime();
        void BulkInsert(List<EfficaVisitInfo> lisattavat);
        void BulkDelete(List<int> efficaVisitInfoKeys);
        void AddEfficaVisit(EfficaVisitInfo visit);
    }
}
