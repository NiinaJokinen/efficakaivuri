﻿using EfficaResultConverter.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess
{
    public interface IEfficaOruRepo
    {
        void BulkInsert(List<EfficaOru> lisattavat);

        List<EfficaOru> GetUnProcessedOrus(int maxKey);

        EfficaOru GetEfficaOru(int key);
        void UpdateEfficaOruProcessed(int oruKey);
    }
}
