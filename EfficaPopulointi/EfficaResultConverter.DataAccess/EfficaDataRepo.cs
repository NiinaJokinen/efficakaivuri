﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EfficaResultConverter.DataAccess.Models;
using PetaPoco;

namespace EfficaResultConverter.DataAccess
{
    public class EfficaDataRepo : IEfficaDataRepo
    {
        public Database Db { get; set; }
        public EfficaDataRepo()
        {
            Db = new Database("EfficaDbConnectionString");
        }

        public List<EfficaData> GetResultData(List<int> visitIds)
        {
            return Db.Fetch<EfficaData>(SqlResource.EfficaDataSql, visitIds.ToArray());
        }

        public List<EfficaData> GetNewLabVisitIds(DateTime resultTime)
        {
            string strYear = ConfigurationManager.AppSettings["Vuosi"] != null ? ConfigurationManager.AppSettings["Vuosi"].Trim() : "2019";
            int intYear = Int32.Parse(strYear);
            int nextYear = intYear + 1;
            string strNextYear = nextYear.ToString();

            var maxVisitCount = ConfigurationManager.AppSettings["MaxVisitCount"];
            if (String.IsNullOrWhiteSpace(maxVisitCount))
                maxVisitCount = "10000";

            Sql query = new Sql();
            query.Select($"TOP {maxVisitCount} a.kayntitunniste as 'LabVisitId', max(a.kuittausaika) as 'ResultTime' ");
            query.From("lbvastausarkisto a");
            query.LeftJoin("lbtutkimusohjaus tohj").On("a.tutkimus_va = tohj.tutkimus_va and a.nottoaika between tohj.alkupvm and tohj.loppupvm");
            query.Where("a.kuittausaika > '1990-01-01' ");
            query.Where("a.kontrolli = '' ");
            query.Where("(naytenumero <> '' or (naytenumero = '' and tohj.tulostuu ='K'))");
            query.Where("(tohj.tulostuu = 'K' or tohj.tilastoituu not in ('0', 'E') or hinta > 0.1)");
            query.Where("isnull(a.kayntitunniste, '') <> '' ");
            query.Where($"a.nottoaika >= '{strYear}-01-01' ");
            //query.Where($"a.nottoaika < '{strYear}-03-01' ");
            query.Where($"a.nottoaika < '{strNextYear}-01-01' ");
            if (resultTime != DateTime.MinValue)
                query.Where("a.kuittausaika > @0 ", resultTime);
            query.GroupBy("a.kayntitunniste");
            query.OrderBy("max(a.kuittausaika)");
            return Db.Fetch<EfficaData>(query);
        }

    }
}
