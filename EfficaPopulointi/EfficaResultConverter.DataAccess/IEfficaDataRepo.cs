﻿using EfficaResultConverter.DataAccess.Models;
using System;
using System.Collections.Generic;

namespace EfficaResultConverter.DataAccess
{
    public interface IEfficaDataRepo
    {
        List<EfficaData> GetResultData(List<int> visitIds);

        List<EfficaData> GetNewLabVisitIds(DateTime resultTime);

    }
}