﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EfficaResultConverter.DataAccess.Models;
using PetaPoco;

namespace EfficaResultConverter.DataAccess
{
    public class EfficaVisitRepo : IEfficaVisitRepo
    {
        public Database Db { get; set; }

        public EfficaVisitRepo()
        {
            Db = new Database("EfficaDbConnectionString");
        }

        // EfficaVisitInfo
        public List<EfficaVisitInfo> GetEfficaVisits(string status)
        {
            //var maxVisitCount = ConfigurationManager.AppSettings["MaxVisitCount"];
            //if (String.IsNullOrWhiteSpace(maxVisitCount))
            //    maxVisitCount = "100";
            // query.Select($"TOP {maxVisitCount} EfficaVisitInfoKey, LabVisitId, Status, StartTime, EndTime");
            Sql query = new Sql();
            query.Select($"EfficaVisitInfoKey, LabVisitId, ResultTime, Status, StartTime, EndTime");
            query.From("EfficaVisitInfo");
            query.Where("Status = @0 ", status);
            return Db.Fetch<EfficaVisitInfo>(query);
        }

        public DateTime? GetMaxResultTime()
        {
            Sql query = new Sql();
            query.Select($"max(ResultTime) ");
            query.From("EfficaVisitInfo");
            return Db.FirstOrDefault<DateTime?>(query);
        }

        public void BulkDelete(List<int> efficaVisitInfoKeys)
        {
            Sql query = new Sql();

            foreach (int key in efficaVisitInfoKeys)
            {
                query.Append("DELETE FROM EfficaVisitInfo WHERE EfficaVisitInfoKey = @0; ", key);
                //query.Append("Delete from EfficaVisitInfo where EfficaVisitInfoKey in (@0) ", efficaVisitInfoKeys.ToArray());
            }
            int monta = Db.Execute(query);

            if(monta != efficaVisitInfoKeys.Count)
            {
                throw new Exception("Kaikkien poisto ei onnistunut");
            }
        }

        //
        public void BulkInsert(List<EfficaVisitInfo> lisattavat)
        {
            BulkOperations.SqlBulkInsert<EfficaVisitInfo>(lisattavat, "EfficaVisitInfo", ConfigurationManager.ConnectionStrings["EfficaDbConnectionString"].ConnectionString);
        }

        public EfficaVisitInfo GetEfficaVisit(int visitId)
        {
            return Db.FirstOrDefault<EfficaVisitInfo>("where LabVisitId = @0 ", visitId);
        }

        public void UpdateEfficaVisit(EfficaVisitInfo visit)
        {
            Db.Update(visit);
        }

        public void AddEfficaVisit(EfficaVisitInfo visit)
        {
            Db.Insert(visit);
        }
    }
}
