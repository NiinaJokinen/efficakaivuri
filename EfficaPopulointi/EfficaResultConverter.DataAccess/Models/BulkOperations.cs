﻿using FastMember;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess.Models
{
    public class BulkOperations
    {
        /// <summary>
        /// Muodostetaan luokasta datatable bulk-inserttiä varten
        /// </summary>
        /// <typeparam name="T">Lisättävän listan petapoco</typeparam>
        /// <param name="lista">Listaobjekti</param>
        public static void SqlBulkInsert<T>(List<T> lista, string tietokantataulu, string connStr)
        {
            List<String> strings = new List<String>();
            using (SqlConnection connection = new SqlConnection(connStr)) //ConfigurationManager.ConnectionStrings["Kaytossa"].ConnectionString
            {
                connection.Open();
                using (SqlBulkCopy bulk = new SqlBulkCopy(connection))
                {
                    bulk.BulkCopyTimeout = 100;
                    bulk.DestinationTableName = connection.Database + ".dbo." + tietokantataulu;
                    DataTable data = new DataTable();
                    // muodostetaan datatablen sarakkeet luokan arvoista joilla on column-property
                    var temp = typeof(T).GetProperties();
                    IEnumerable<PropertyInfo> kentat = typeof(T).GetProperties().Where(prop => !prop.IsDefined(typeof(ResultColumnAttribute), false));
                    foreach (PropertyInfo kentta in kentat)
                    {
                        // varaudutaan siihen, että on nullable
                        Type columnType = kentta.PropertyType;
                        if (kentta.PropertyType.IsGenericType && kentta.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            // If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
                            columnType = kentta.PropertyType.GetGenericArguments()[0];
                        }

                        DataColumn column = new DataColumn(kentta.Name, columnType);
                        data.Columns.Add(column);
                        bulk.ColumnMappings.Add(kentta.Name, kentta.Name);
                    }
                    // hae arvot ja lisää niistä uusi rivi
                    foreach (T t in lista)
                    {
                        object[] arvot = typeof(T).GetProperties().Where(prop => !prop.IsDefined(typeof(ResultColumnAttribute), false)).Select(k => k.GetValue(t, null)).ToArray<object>();
                        strings.Add(arvot[0].ToString());
                        data.Rows.Add(arvot);
                    }
                    bulk.WriteToServer(data);
                }
            }
        }
    }
}
