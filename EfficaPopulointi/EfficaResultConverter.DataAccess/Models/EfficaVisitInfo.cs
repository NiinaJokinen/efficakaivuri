﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess
{
    [TableName("EfficaVisitInfo")]
    [PrimaryKey("EfficaVisitInfoKey", AutoIncrement = true)]
    public class EfficaVisitInfo
    {
        public int EfficaVisitInfoKey { get; set; }
        public int LabVisitId { get; set; }
        public DateTime ResultTime { get; set; }
        public string Status { get; set; } // Vakiotilat: Käsittelemätön/Käsittelyssä/Siirretty/Virhe
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Error { get; set; }
    }
}
