﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess.Models
{
    public class EfficaData
    {
        public string PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int LabVisitId { get; set; }
        public string SpecimenNumber { get; set; }
        public int WorkId { get; set; } // tytunniste
        public DateTime SpecimenTime { get; set; }
        public string ResearchId { get; set; }
        public string ResearchDef { get; set; }
        public string ResearchText { get; set; }
        public string ResearchField { get; set; }
        public string Result { get; set; }
        public string AbnormalFlag { get; set; }
        public string Unit { get; set; }
        public string RefValue { get; set; }
        public string Notification { get; set; }
        public string Statement { get; set; }
        public string MainResearchId { get; set; }
        public string MainResearchDef { get; set; }
        public string Rasitusaika { get; set; }
        public DateTime ResultTime { get; set; }
        public string OrderDoctor { get; set; }
        public string ResultDoctor { get; set; }
        public string OrderOrganization { get; set; }
        public string ResultOrganization { get; set; }
        public string OrgId { get; set; }
        public string OrgNumber { get; set; }
        public string Urgency { get; set; }
        public string OrgUnit { get; set; }
    }
}
