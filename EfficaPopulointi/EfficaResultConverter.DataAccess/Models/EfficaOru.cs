﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess.Models
{
    [TableName("EfficaOru")]
    [PrimaryKey("EfficaOruKey", AutoIncrement = false)]
    public class EfficaOru
    {
        public int EfficaOruKey { get; set; }
        public int LabVisitId { get; set; }
        public string SpecimenNumber { get; set; }
        public string HL7 { get; set; }
        public bool Processed { get; set; }
        public DateTime ProcessedTime { get; set; }

    }
}
