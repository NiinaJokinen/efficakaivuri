select 
a.henkilotunnus as 'PersonId', v.etunimi as 'FirstName', v.sukunimi as 'LastName', a.kayntitunniste as 'LabVisitId', a.naytenumero as 'SpecimenNumber', a.tytunniste as 'WorkId', a.nottoaika as 'SpecimenTime', 
a.tutkno as 'ResearchId', a.tutkly as 'ResearchDef', n.tunnusteksti as 'ResearchText', a.tut_erikala1 as 'ResearchField', tohj.jsuhte as 'suht.j�rj', 
a.san_tulos as 'Result', a.ylitys as 'AbnormalFlag', a.yksikko as 'Unit', a.viitearvo as 'RefValue', a.huomautus as 'Notification', b.lausunto as 'Statement', 
a.paatutkimus_nro as 'MainResearchId', a.paatutkimus_lyhenne as 'MainResearchDef', a.rasitusaika as 'Rasitusaika', a.kuittausaika as 'ResultTime', 
a.tilaava_laakari as 'OrderDoctor', a.tulos_laakari as 'ResultDoctor', a.tulkpl as 'ResultOrganization', tk.virallinentunnus as 'OrgId', tk.tkno as 'OrgNumber', a.kiireellisyys as 'Urgency', a.tilkpl as 'OrgUnit'
from lbvastausarkisto a
left join lblausunto b on a.tytunniste = b.tytunniste 
left join lbtutkimusohjaus tohj on a.tutkimus_va = tohj.tutkimus_va and a.nottoaika between tohj.alkupvm and tohj.loppupvm
left join lbtutkimustunnus n on a.tutkimus_va = n.tutkimus_va and n.tunnuslaji = 'NIMI' and a.nottoaika between n.alkupvm and n.loppupvm 
left join co_suorituspaikka sp on sp.lyhenne = a.tilkpl
left join CO_TERVEYSKESKUS tk on tk.tkno = sp.terveyskeskus
left join co_vaesto v on a.henkilotunnus = v.htun 
where a.kuittausaika > '1990-01-01' 
and a.kayntitunniste in (@0)
and a.kontrolli='' -- kontrollit pois
and (naytenumero <> '' or (naytenumero = '' and tohj.tulostuu ='K')) -- sallitaan vain sellaiset n�ytenumerottomat, joilla tulostuu on K
and (tohj.tulostuu ='K' or tohj.tilastoituu not in ('0', 'E') or hinta > 0.1) -- vain tulostettavat tai tilastoituvat tai laskutettavat (ei apututukimuksia)
--and NOT (a.san_tulos = 'Alustava' AND a.tut_erikala1 = 'P') -- alustava ei ole vastaus, vaan �vastausmuodossa� oleva tieto ett� n�ytteen k�sittely on aloitettu -- patologiaa ei k�sitell� t�t� kautta ollenkaan
order by a.nottoaika desc, a.naytenumero, a.tytunniste asc