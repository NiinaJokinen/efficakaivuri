select 
a.henkilotunnus as 'PersonId', v.etunimi as 'FirstName', v.sukunimi as 'LastName', a.kayntitunniste as 'LabVisitId', a.naytenumero as 'SpecimenNumber', a.tytunniste as 'WorkId', a.nottoaika as 'SpecimenTime', 
a.tutkno as 'ResearchId', a.tutkly as 'ResearchDef', n.tunnusteksti as 'ResearchText', a.tut_erikala1 as 'ResearchField', tohj.jsuhte as 'suht.j�rj', 
a.san_tulos as 'Result', a.ylitys as 'AbnormalFlag', a.yksikko as 'Unit', a.viitearvo as 'RefValue', a.huomautus as 'Notification', b.lausunto as 'Statement', 
a.paatutkimus_nro as 'MainResearchId', a.paatutkimus_lyhenne as 'MainResearchDef', a.rasitusaika as 'Rasitusaika', a.kuittausaika as 'ResultTime', 
a.tilaava_laakari as 'OrderDoctor', a.tulos_laakari as 'ResultDoctor', a.tulkpl as 'ResultOrganization', tk.virallinentunnus as 'OrgId', tk.tkno as 'OrgNumber', a.kiireellisyys as 'Urgency', a.tilkpl as 'OrgUnit'
from lbvastausarkisto a
left join lblausunto b on a.tytunniste = b.tytunniste 
left join lbtutkimusohjaus tohj on a.tutkimus_va = tohj.tutkimus_va and a.nottoaika between tohj.alkupvm and tohj.loppupvm
left join lbtutkimustunnus n on a.tutkimus_va = n.tutkimus_va and n.tunnuslaji = 'NIMI' and a.nottoaika between n.alkupvm and n.loppupvm 
left join co_suorituspaikka sp on sp.lyhenne = a.tilkpl
left join CO_TERVEYSKESKUS tk on tk.tkno = sp.terveyskeskus
left join co_vaesto v on a.henkilotunnus = v.htun 
where a.kuittausaika > '2010-01-01' 
and a.kayntitunniste in (@0)
and a.kontrolli='' -- kontrollit pois
and naytenumero <> '' -- pussikohtaiset X-kokeet pois
and (tohj.tulostuu ='K' or tohj.tilastoituu not in ('0', 'E') or hinta > 0.1) -- vain tulostettavat tai tilastoituvat tai laskutettavat (ei apututukimuksia)
and NOT (a.san_tulos = 'Alustava' AND a.tut_erikala1 = 'P') -- alustava ei ole vastaus, vaan �vastausmuodossa� oleva tieto ett� n�ytteen k�sittely on aloitettu
and a.tutkno IN ('2575', '3053', '3860', '3990', '4034', '4035', '4037', '4038', '4039', '4040', '4043', '4044', '4046', '4047', '4049', '4050', '4051', '4052', '4053', '4054', '4055', '4056', '4057', '4059', '4060', '4061', '4064', '4065', '4066', '4067', '4069', '4070', '4071', '4072', '4076', '4077', 
'4078', '4079', '4080', '4081', '4184', '4185', '4187', '4188', '4189', '4191', '4191', '4194', '4424', '4721', '4763', '4764', '4765', '4765', '6144', '6146', '6147', '6148', '6149', '6274', '9826', '9870', '9871', '9991', '10711', '10841', '10841', '10842', '10878', '10886', '10887', '10888', '10893', 
'11050', '11051', '11118', '11119', '11174', '11175', '57021', '57022', '57023', '57024', '57025', '4054A')
order by a.nottoaika desc, a.naytenumero, a.tytunniste asc