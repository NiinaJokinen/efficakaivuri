﻿using EfficaResultConverter.DataAccess.Models;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.DataAccess
{
    public class EfficaOruRepo : IEfficaOruRepo
    {
        public Database Db { get; set; }

        public EfficaOruRepo()
        {
            Db = new Database("EfficaDbConnectionString");
        }

        public List<EfficaOru> GetUnProcessedOrus(int maxKey)
        {
            var maxOruCount = ConfigurationManager.AppSettings["MaxOruCount"];
            if (String.IsNullOrWhiteSpace(maxOruCount))
                maxOruCount = "10000";

            Sql query = new Sql();
            query.Select($"TOP {maxOruCount} EfficaOruKey, LabVisitId, SpecimenNumber, HL7, Processed, ProcessedTime ");
            query.From("EfficaOru");
            query.Where("EfficaOruKey > @0", maxKey);
            return Db.Fetch<EfficaOru>(query);
        }

        public EfficaOru GetEfficaOru(int key)
        {
            return Db.FirstOrDefault<EfficaOru>("where EfficaOruKey = @0 ", key);
        }

        public void UpdateEfficaOruProcessed(int oruKey)
        {
            Db.Execute($"Update EfficaOru set Processed = 1, ProcessedTime = getdate() where EfficaOruKey = {oruKey}");
        }

        //
        public void BulkInsert(List<EfficaOru> lisattavat)
        {
            BulkOperations.SqlBulkInsert<EfficaOru>(lisattavat, "EfficaOru", ConfigurationManager.ConnectionStrings["EfficaDbConnectionString"].ConnectionString);
        }
    }
}
