﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfficaResultConverter.Model
{
    public class Specimen
    {
        public string SpecimenNumber { get; set; }
        public DateTime SpecimenTime { get; set; }
    }
}
